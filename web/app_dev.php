<?php


error_reporting(E_ALL);

use Symfony\Component\HttpFoundation\Request;


// If you don't want to setup permissions the proper way, just uncomment the following PHP line
// read http://symfony.com/doc/current/book/installation.html#configuration-and-setup for more information
//umask(0000);

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.


require_once __DIR__.'/../app/bootstrap.php.cache';

require_once __DIR__.'/../app/AppKernel.php';
require_once __DIR__.'/../app/AppCache.php';


$kernel = new AppKernel('dev', false);

$kernel->loadClassCache();

$kernel->handle(Request::createFromGlobals())->send();
