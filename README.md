Enigma-Switch-Web
========================

                 _
             ,.-" "-.,
            /   ===   \
           /  =======  \
        __|  (o)   (0)  |__
       / _|    .---.    |_ \
      | /.----/ O O \----.\ |
       \/     |     |     \/
       |                   |
       |                   |
       |                   |
       _\   -.,_____,.-   /_

OpenSIPS web interface

IN PROGRESS:

BungleChatBundle: Websocket based chat service. - Mark Dalby

BungleRegistrarBundle: SIP Registrar and user handling - Paul Hammond

BungleUserBundle: Addition of peer / USRLOC information to a FOSUserBundle entity. - Mark Dalby

Next Meeting:

01/03/2013 - Sorry for long period, Mark D and Ben J are on holiday