class php54 {
  $packages = ["php5", "php5-cli", "php-pear", "php5-dev", "php5-gd", "php5-curl", "mongodb"]

	file { "/etc/apt/sources.list":
		ensure => file,
		owner => root,
		group => root,
		source => "puppet:///modules/php54/sources.list",
	}

	exec { "import-gpg":
		command => "/usr/bin/wget -q http://www.dotdeb.org/dotdeb.gpg -O -| /usr/bin/apt-key add -"
	}

	exec { "/usr/bin/apt-get update":
		require => [File["/etc/apt/sources.list"], Exec["import-gpg"]],
	}

	package { [
		$packages
	] :
		ensure => latest,
		require => [Exec["apt-get update"], Exec["/usr/bin/apt-get update"]],
		notify => Service['apache2']
	}

	exec{"install pecl mongo":
	    command => 	"pecl install mongo-1.3.7",
	    unless => "pecl info mongo | grep -c 'Release Version'",
	    require => Package[$packages],
	    path    => ['/usr/bin/', '/bin/']
	}

    file { [ "/etc", "/etc/php5", "/etc/php5/apache2" ]:
       ensure => directory,
       before => [File ['/etc/php5/apache2filter/php.ini'], File["/etc/php5/cli/php.ini"]]
    }

    file{"/etc/php5/apache2filter/php.ini":
        content => template('php54/php.ini.erb'),
        notify => Service['apache2'],
        ensure => present,
        require => [Package['apache2'], Package['php5']]
        }

    file{"/etc/php5/cli/php.ini":
        content => template('php54/php.ini.erb'),
        notify => Service['apache2'],
        ensure => present,
        require => [Package['apache2'], Package['php5']]
        }

}