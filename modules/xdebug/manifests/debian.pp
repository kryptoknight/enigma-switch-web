class xdebug::debian {

    include xdebug::params

    package { "xdebug":
        name   => $xdebug::params::pkg,
        ensure => installed,
        require => Class['php54'],
        notify => Service['apache2'],
    }

    file { '/etc/php5/conf.d/xdebug_config.ini' :
        content => "xdebug.remote_enable=on
                    xdebug.remote_handler=dbgp
                    xdebug.remote_host=33.33.33.1
                    xdebug.remote_port=9000
                    xdebug.remote_connect_back=on
                    xdebug.remote_autostart=0",
        ensure  => present,
        require => Package['xdebug'],
        notify  => Service['apache2'],
    }

}