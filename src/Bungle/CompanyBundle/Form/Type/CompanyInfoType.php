<?php

namespace Bungle\CompanyBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CompanyInfoType extends AbstractType
{

    public function __construct($isAdmin){
        $this->isAdmin = $isAdmin;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver){
        $resolver->setDefaults(array(
            'data_class'      => 'Bungle\CompanyBundle\Document\Company',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'name'=>"companyForm",
        ));
    }   
            
    public function buildForm(FormBuilderInterface $builder, array $options){   

        // admin specific fields
        
        if ($this->isAdmin){
            
            $builder->add('accountNo','text',array(
                'label'=>'Account Number',
                'required'=>false));
                    
            $builder->add('parent','document',array(
                'class'     =>'Bungle\CompanyBundle\Document\Company',
                'property'  =>'name',
                'required'  => false,
                'label'     => 'Parent Company'
            ));
        }
        
        
        // Company Address
        
        $builder->add('name','text',array('label'=>'Company Name'));
        
        $builder->add('addressBuilding','text',array(
            'label'=>'Building Name',
            'required'=>false)
        );
        $builder->add('addressNo','text',array(
            'label'=>'Street Number',
            'required'=>false)
        );
        
        $builder->add('addressStreet','text',array(
            'label'=>'Street Name') 
        );
        
        $builder->add('addressLine2','text',array(
            'label'=>'Address Line 2',
            'required' => false)
        );
        
        $builder->add('addressState','text',array(
            'label'=>'State / County')
        );
        
        $builder->add('addressPostcode','text',array(
            'label'=>'Postal / Zip code')
        );
        
        $builder->add('addressCountry','text',array(
            'label'=>'Country')
        );
        
        // Billing Address
        
        $builder->add('cloneBillingAddress', 'checkbox',array(
            'label'=> "Copy from company address?",
            'mapped'=> false,
            'required'=>false)
        );
        
        $builder->add('billingAddressBuilding','text',array(
            'label'=>'Building Name',
            'required'=>false)
        );
        $builder->add('billingAddressNo','text',array(
            'label'=>'Street Number',
            'required'=>false)
        );
        
        $builder->add('billingAddressStreet','text',array(
            'label'=>'Street Name')
        );
        
        $builder->add('billingAddressLine2','text',array(
            'label'=>'Address Line 2',
            'required'=>false)
        );
        
        $builder->add('billingAddressState','text',array(
            'label'=>'State / County')
        );
        
        $builder->add('billingAddressPostcode','text',array(
            'label'=>'Postal / Zip code')
        );
        
        $builder->add('billingAddressCountry','text',array(
            'label'=>'Country')
        );
    }

    public function getName(){
        return 'bungle_companyInfo_form';
    }
}
?>