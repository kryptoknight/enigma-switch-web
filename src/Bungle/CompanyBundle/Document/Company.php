<?php

namespace Bungle\CompanyBundle\Document;
use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Company 
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
     /**
     * @MongoDB\String @MongoDB\Index(unique="true")
     */
    protected $accountNo;
    /**
     * @MongoDB\String
     */
    protected $created;
    /**
     * @MongoDB\String
     */
    protected $modified;
    
    /**
     * @MongoDB\String @MongoDB\Index(unique="true")
     */
    protected $name;
    
    /**
     * @MongoDB\String
     */
    protected $addressBuilding;  
    
    /**
     * @MongoDB\String
     */
    protected $addressNo;  
    
    /**
     * @MongoDB\String
     */
    protected $addressStreet;  
    
    /**
     * @MongoDB\String
     */
    protected $addressLine2;   
    
    /**
     * @MongoDB\String
     */
    protected $addressState;  
    
    /**
     * @MongoDB\String
     */
    protected $addressPostcode;   
    
    /**
     * @MongoDB\String
     */
    protected $addressCountry;   
    
   /**
     * @MongoDB\String
     */
    protected $billingAddressBuilding;    
    
    /**
     * @MongoDB\String
     */
    protected $billingAddressNo;   
    
    /**
     * @MongoDB\String
     */
    protected $billingAddressStreet;  
    
    /**
     * @MongoDB\String
     */
    protected $billingAddressLine2;   
    
    /**
     * @MongoDB\String
     */
    protected $billingAddressState;  
    
    /**
     * @MongoDB\String
     */
    protected $billingAddressPostcode;   
    
    /**
     * @MongoDB\String
     */
    protected $billingAddressCountry;   
    
    /**
     * @MongoDB\Id
     * @MongoDB\ReferenceOne(targetDocument="Bungle\CompanyBundle\Document\Company")
     */
    protected $parent;
 
    /**
     * Get the account number of this company.
     * @return string
     */
    public function getAccountNo(){
        return $this->accountNo;
    }
    /**
     * Set the account number of this company.
     * @param string $accountNo
     */
    public function setAccountNo($accountNo){
        $this->accountNo = $accountNo;
    }  
   /**
     * Set the account number of this company on creation.
     * @MongoDB\PrePersist
     */
    public function checkAccountNo(){
        if(empty($this->accountNo)){
            $this->accountNo= rand(22222222,99999999);
         } 
    }
        
    /**
     * Get the creation date of this company.
     * @return String
     */
    public function getCreated(){
        return $this->created;
    }
    
    /**
     * Set the creation date of this company.
     * @MongoDB\PrePersist
     */
    public function setCreated(){
            $this->created = date('j-m-Y g:i:s a'); 
    }   
    
    /**
     * Get the modification date of this company.
     * @return String
     */
    public function getModified(){
        return $this->modified;
    }
    
    /**
     * Set the modification date of this company.
     * @MongoDB\PreUpdate
     */
    public function setModified(){
        $this->modified = date('j-m-Y g:i:s a'); 
    }      

    /**
     * Get the name of this company.
     * @return string
     */
    public function getName(){
        return $this->name;
    }
    
    /**
     * Set the name of this company.
     * @param string $name
     */
    public function setName($name){
        $this->name = $name;
    }
    
    /**
     * Get the Street Number of this company.
     * @return string
     */
    public function getAddressNo(){
        return $this->addressNo;
    }
    /**
     * Set the Address Number of this company.
     * @param string $addressNo
     */
    public function setAddressNo($addressNo){
        $this->addressNo = $addressNo;
    }    
    /**
     * Return the parent company as a MongoDB Document.
     * @return MongoDbDocument $parent
     */
    public function getParent(){
        return $this->parent;
    }
    /**
     * Set the parent company.
     * @param MongoDBDocument $parent
     */
    public function setParent($parent){
        $this->parent = $parent;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set addressStreet
     *
     * @param string $addressStreet
     * @return \Company
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;
        return $this;
    }

    /**
     * Get addressStreet
     *
     * @return string $addressStreet
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * Set addressLine2
     *
     * @param string $addressLine2
     * @return \Company
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
        return $this;
    }

    /**
     * Get addressLine2
     *
     * @return string $addressLine2
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * Set addressState
     *
     * @param string $addressState
     * @return \Company
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;
        return $this;
    }

    /**
     * Get addressState
     *
     * @return string $addressState
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * Set addressPostcode
     *
     * @param string $addressPostcode
     * @return \Company
     */
    public function setAddressPostcode($addressPostcode)
    {
        $this->addressPostcode = $addressPostcode;
        return $this;
    }

    /**
     * Get addressPostcode
     *
     * @return string $addressPostcode
     */
    public function getAddressPostcode()
    {
        return $this->addressPostcode;
    }

    /**
     * Set addressCountry
     *
     * @param string $addressCountry
     * @return \Company
     */
    public function setAddressCountry($addressCountry)
    {
        $this->addressCountry = $addressCountry;
        return $this;
    }

    /**
     * Get addressCountry
     *
     * @return string $addressCountry
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }

    /**
     * Set addressBuilding
     *
     * @param string $addressBuilding
     * @return \Company
     */
    public function setAddressBuilding($addressBuilding)
    {
        $this->addressBuilding = $addressBuilding;
        return $this;
    }

    /**
     * Get addressBuilding
     *
     * @return string $addressBuilding
     */
    public function getAddressBuilding()
    {
        return $this->addressBuilding;
    }

    /**
     * Set billingAddressBuilding
     *
     * @param string $billingAddressBuilding
     * @return \Company
     */
    public function setBillingAddressBuilding($billingAddressBuilding)
    {
        $this->billingAddressBuilding = $billingAddressBuilding;
        return $this;
    }

    /**
     * Get billingAddressBuilding
     *
     * @return string $billingAddressBuilding
     */
    public function getBillingAddressBuilding()
    {
        return $this->billingAddressBuilding;
    }

    /**
     * Set billingAddressNo
     *
     * @param string $billingAddressNo
     * @return \Company
     */
    public function setBillingAddressNo($billingAddressNo)
    {
        $this->billingAddressNo = $billingAddressNo;
        return $this;
    }

    /**
     * Get billingAddressNo
     *
     * @return string $billingAddressNo
     */
    public function getBillingAddressNo()
    {
        return $this->billingAddressNo;
    }

    /**
     * Set billingAddressStreet
     *
     * @param string $billingAddressStreet
     * @return \Company
     */
    public function setBillingAddressStreet($billingAddressStreet)
    {
        $this->billingAddressStreet = $billingAddressStreet;
        return $this;
    }

    /**
     * Get billingAddressStreet
     *
     * @return string $billingAddressStreet
     */
    public function getBillingAddressStreet()
    {
        return $this->billingAddressStreet;
    }

    /**
     * Set billingAddressLine2
     *
     * @param string $billingAddressLine2
     * @return \Company
     */
    public function setBillingAddressLine2($billingAddressLine2)
    {
        $this->billingAddressLine2 = $billingAddressLine2;
        return $this;
    }

    /**
     * Get billingAddressLine2
     *
     * @return string $billingAddressLine2
     */
    public function getBillingAddressLine2()
    {
        return $this->billingAddressLine2;
    }

    /**
     * Set billingAddressState
     *
     * @param string $billingAddressState
     * @return \Company
     */
    public function setBillingAddressState($billingAddressState)
    {
        $this->billingAddressState = $billingAddressState;
        return $this;
    }

    /**
     * Get billingAddressState
     *
     * @return string $billingAddressState
     */
    public function getBillingAddressState()
    {
        return $this->billingAddressState;
    }

    /**
     * Set billingAddressPostcode
     *
     * @param string $billingAddressPostcode
     * @return \Company
     */
    public function setBillingAddressPostcode($billingAddressPostcode)
    {
        $this->billingAddressPostcode = $billingAddressPostcode;
        return $this;
    }

    /**
     * Get billingAddressPostcode
     *
     * @return string $billingAddressPostcode
     */
    public function getBillingAddressPostcode()
    {
        return $this->billingAddressPostcode;
    }

    /**
     * Set billingAddressCountry
     *
     * @param string $billingAddressCountry
     * @return \Company
     */
    public function setBillingAddressCountry($billingAddressCountry)
    {
        $this->billingAddressCountry = $billingAddressCountry;
        return $this;
    }

    /**
     * Get billingAddressCountry
     *
     * @return string $billingAddressCountry
     */
    public function getBillingAddressCountry()
    {
        return $this->billingAddressCountry;
    }    
}
