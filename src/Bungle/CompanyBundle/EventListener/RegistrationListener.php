<?php

namespace Bungle\CompanyBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RegistrationListener implements EventSubscriberInterface {
    
    private $container;
    
    public function __construct($container){
        $this->container = $container;
    }
    
    public static function getSubscribedEvents(){
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => 'onSuccess',
        );
    }

    public function onSuccess($event){
          $user = $event->getForm()->getData();
          $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
          $CompanyRepo = $dm->getRepository('Bungle\CompanyBundle\Document\Company');   
          $company = $CompanyRepo->findOneBy(
                  array('name'=>$this->container->getParameter('bungle_company.default_company')));
          $user->setCompany($company);
    }    
}

?>
