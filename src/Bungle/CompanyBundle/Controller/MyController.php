<?php

namespace Bungle\CompanyBundle\Controller;
// Bungle Classes
use Bungle\CompanyBundle\Form\Type\CompanyInfoType;
use Bungle\CompanyBundle\Document\Company as Company;
// Symfony Components
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException as AccessDeniedException;
// Symfony Bundles
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// Sensio Components
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class MyController extends Controller
{
    /**
     * View the information of the company curently assigned to your user account.
     * @Route("/my/")
     * @Route("/", name="bungle_company_my_view")
     * @Template()
     */
    public function indexAction()
    {
        return array('name' => "asd");
    }
    
    /**
     * Edit the company assigned with your account.
     * @Route("/edit", name="bungle_company_my_edit")
     * @Template()
     */
    public function editAction(Request $request){
        $user = $this->container->get('security.context');
        if ($user->isGranted('ROLE_COMPANY_ADMIN')){
            $myCompany = $user->getToken()->getUser()->getCompany();
            $form = $this->createForm(new CompanyInfoType(
                    $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')
            ), $myCompany);
        if ($request->getMethod() == 'POST') {
    		$form->bindRequest($request);
                if($form->isValid()){
                    $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
                    $dm->persist($myCompany);
                    $dm->flush();
                    $this->get('session')->setFlash('system succes', "Your company information has been updated!");
                    return $this->redirect($this->generateUrl('bungle_company_my_view'));
    		}else{
                    foreach ($form->getErrors() as $error){
                        $this->get('session')->setFlash('system error', $error->getmessageTemplate());
                    }
                }
         }
            return array('form' => $form->createView());
        }else{
            throw new AccessDeniedException;
        }
    }
}

