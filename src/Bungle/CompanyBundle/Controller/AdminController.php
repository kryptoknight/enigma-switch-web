<?php

namespace Bungle\CompanyBundle\Controller;
// Bungle Classes
use Bungle\CompanyBundle\Form\Type\CompanyInfoType;
use Bungle\CompanyBundle\Document\Company as Company;
// Symfony Components
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException as AccessDeniedException;
// Symfony Bundles
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// Sensio Components
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AdminController extends Controller
{
    /**
     * Add a new company (Admin Only)
     * @Route("/admin/add", name="bungle_company_admin_add")
     * @Template("BungleCompanyBundle:Admin:add.html.twig")
     */
    public function addAction(Request $request){
        if ($this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
        $company = new Company();
        $form = $this->createForm(new CompanyInfoType(true), $company);
        if ($request->getMethod() == 'POST') {
    		$form->bindRequest($request);
    		$form->getData();
    		if($form->isValid()){
                    $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
                    $dm->persist($company);
                    $dm->flush();
                    $this->get('session')->setFlash(
                            'system succes', 
                            "A new company has been added!"
                    );
                    return $this->redirect($this->generateUrl('bungle_company_admin'));
    		}
        }
        return array('form' => $form->createView());   
        }else{
            throw new AccessDeniedException;
        }      
    }
    /**
     * Company administration index (Admin Only)
     * @Route("/admin/", name="bungle_company_admin")
     * @Route("/admin", name="bungle_company_admin_noslash")
     * @Template("BungleCompanyBundle:Admin:index.html.twig")
     */
    public function indexAction(){
        $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
        $companyRepo = $dm->getRepository('BungleCompanyBundle:Company');
        $companies = $companyRepo->findBy(array());
        $results = array();
        foreach ($companies as $company){
            
            if ($company->getParent()){
                $parent = $company->getParent()->getName();
            }else{
                $parent = null;
            }
           
            $results[] = array(
                'account'=>$company->getAccountNo(),
                'name'=>$company->getName(),
                'created'=>$company->getCreated(),
                'parent'=>$parent,
                'modified'=>$company->getModified()
                );
        }

        return array('results' => $results);  
    }
    
        /**
     * Edit any company (God Mode)
     * @Route("/admin/edit/{accountNo}", name="bungle_company_admin_edit")
     * @Template()
     */
    public function editAction($accountNo, Request $request){
        $user = $this->container->get('security.context');
        $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
        if ($user->isGranted('ROLE_SUPER_ADMIN')){
            $companyRepo = $dm->getRepository('BungleCompanyBundle:Company');
            $Company = $companyRepo->findOneBy(array('accountNo'=>$accountNo));
            $form = $this->createForm(new CompanyInfoType(true), $Company);
        if ($request->getMethod() == 'POST') {
    		$form->bindRequest($request);
                if($form->isValid()){
                    $dm->persist($Company);
                    $dm->flush();
                    $this->get('session')->setFlash('system succes', "Company information has been updated!");
                    return $this->redirect($this->generateUrl('bungle_company_admin'));
    		}else{
                    foreach ($form->getErrors() as $error){
                        $this->get('session')->setFlash('system error', $error->getmessageTemplate());
                    }
                }
         }
            return array('form' => $form->createView(), 'accountNo'=>$accountNo);
        }else{
            throw new AccessDeniedException;
        }
    }
}
?>
