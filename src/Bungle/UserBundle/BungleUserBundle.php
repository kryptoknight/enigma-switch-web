<?php

namespace Bungle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BungleUserBundle extends Bundle{
    public function getParent(){
        return 'FOSUserBundle';
    }
}
