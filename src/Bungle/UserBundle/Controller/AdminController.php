<?php

namespace Bungle\UserBundle\Controller;
// Bungle Classes
use Bungle\UserBundle\Form\Type\UserInfoType;
use Bungle\UserBundle\Document\User as User;
// Symfony Components
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException as AccessDeniedException;
// Symfony Bundles
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// Sensio Components
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AdminController extends Controller
{
    /**
     * Add a new user (Admin Only)
     * @Route("/admin/add", name="bungle_user_admin_add")
     * @Template("BungleUserBundle:Admin:add.html.twig")
     */
    public function addAction(Request $request){
        if ($this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
        $user = new User();
        $form = $this->createForm(new UserInfoType($this->container->get('security.context')), $user);
        if ($request->getMethod() == 'POST') {
    		$form->bindRequest($request);
    		$form->getData();
    		if($form->isValid()){
                    $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
                    $dm->persist($user);
                    $dm->flush();
                    $this->get('session')->setFlash(
                            'system succes', 
                            "A new user has been added!"
                    );
                    return $this->redirect($this->generateUrl('bungle_user_admin'));
    		}
        }
        return array('form' => $form->createView());   
        }else{
            throw new AccessDeniedException;
        } 
    }
    /**
     * Add a new user (Admin Only)
     * @Route("/admin", name="bungle_user_admin")
     * @Route("/admin/", name="bungle_user_admin_slash")
     * @Template("BungleUserBundle:Admin:index.html.twig")
     */
    public function indexAction(){
        $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
        $userRepo = $dm->getRepository('BungleUserBundle:User');
        $users = $userRepo->findBy(array());
        $results = array();
        foreach ($users as $user){
            if ($user->getCompany()){
                $companyName = $user->getCompany()->getName();
            }else{
                $companyName = null;
            }
            $results[] = array(
                'username'=>$user->getUsername(),
                'name'=>$user->getName(),
                'companyName'=>$companyName,
                'lastLogin'=>$user->getLastLogin()->format('Y-m-d H:i:s'),
                'roles'=>$user->getRoles()
                );
        }

        return array('results' => $results);  
    }
    /**
     * Add a new user (Admin Only)
     * @Route("/admin/edit/{username}", name="bungle_user_admin_edit")
     * @Template("BungleUserBundle:Admin:edit.html.twig")
     */
    public function editAction($username, Request $request){
        $user = $this->container->get('security.context');
        $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
        if ($user->isGranted('ROLE_SUPER_ADMIN')){
            $userRepo = $dm->getRepository('BungleUserBundle:User');
            $selectedUser = $userRepo->findOneBy(array('username'=>$username));
            $form = $this->createForm(new UserInfoType($user), $selectedUser);
        if ($request->getMethod() == 'POST') {
    		$form->bindRequest($request);
                if($form->isValid()){
                    $dm->persist($selectedUser);
                    $dm->flush();
                    $this->get('session')->setFlash('system succes', "User information has been updated!");
                    return $this->redirect($this->generateUrl('bungle_user_admin'));
    		}else{
                    foreach ($form->getErrors() as $error){
                        $this->get('session')->setFlash('system error', $error->getmessageTemplate());
                    }
                }
         }
            return array('form' => $form->createView(), 'username'=>$username);
        }else{
            throw new AccessDeniedException;
        }
    }  
}