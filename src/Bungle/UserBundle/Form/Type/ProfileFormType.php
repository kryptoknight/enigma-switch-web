<?php

namespace Bungle\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;

class ProfileFormType extends BaseType{
        
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('name', 'text', array('label'=>"Full Name"));
        $builder->add('email', 'email', array('label'=>"Email Address"));
        $builder->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'first_options' => array('label' => 'New Password'),
                'second_options' => array('label' => 'Retype Password'),
                'invalid_message' => 'Passwords do not match',
                'required'=>false
        ));
    }

    public function getName(){
        return 'bungle_user_profile';
    }
}
?>
