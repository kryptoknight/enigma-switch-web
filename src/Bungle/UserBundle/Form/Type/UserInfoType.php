<?php

namespace Bungle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ODM\MongoDB\Query\Builder as builder;
use Bungle\CompanyBundle\Repository\CompanyRepository as CompanyRepo;

class UserInfoType extends AbstractType{
    
    protected $user;
    
    public function __construct($user){
        $this->user = $user;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        
        if($this->user->isGranted('ROLE_SUPER_ADMIN')){
            $builder->add('Company','document',array(
                'class'     => 'Bungle\CompanyBundle\Document\Company',
                'query_builder' => function($repository) {
                    return $repository->createQueryBuilder('c')
                            ->field("accountNo")
                            ->equals('123456');
                },
                'property'  => 'name',
                'read_only' => false,
                'disabled'  => false,
                'label'     => "Company",
            ));
        }
        
        $builder->add('username', 'text', array('label'=>"Username"));
        $builder->add('name', 'text', array('label'=>"Full Name"));
        $builder->add('email', 'email', array('label'=>"Email Address"));
        $builder->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'first_options' => array('label' => 'New Password'),
                'second_options' => array('label' => 'Retype Password'),
                'invalid_message' => 'Passwords do not match',
                'required'=>false
        ));
    }

    public function getName(){
        return 'bungle_admin_userinfo';
    }
}
?>