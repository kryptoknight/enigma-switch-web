<?php

namespace Bungle\UserBundle\Document;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 * @MongoDB\Document(repositoryClass="Bungle\UserBundle\Repository\UserRepository")
 */

class User extends BaseUser
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;
    
    /**
     * Id
     * @MongoDB\ReferenceOne(targetDocument="Bungle\CompanyBundle\Document\Company")
     */
    protected $company;
    
    /**
     * @MongoDB\String 
     * @MongoDB\Index(unique=true)
     */
    public $name;
    
    public function __construct(){
        parent::__construct();
    }
    /**
     * Get the name of this user.
     */
    public function getName(){
        return $this->name;
    }
    /**
     * Set the company  for this user.
     * @param MongoDBDocument $company
     */
    public function setName($name){
       $this->name = $name;
    }
    /**
     * Get the company this user belongs to.
     * @return a mongodb company document.
     */
    public function getCompany(){
        return $this->company;
    }
    /**
     * Set the company  for this user.
     * @param MongoDBDocument $company
     */
    public function setCompany($company){
       $this->company = $company;
    }
    /**
     * Get the Lastlogin foir this user.
     * @return a mongodb DateTime object or a string if non is set.
     */   
    public function getLastLogin(){
        if ($this->lastLogin){
            return $this->lastLogin;
        }else{
            return new \DateTime('2000-01-01');
        }
        
    }

}
?>
