<?php

namespace Bungle\ChatBundle\EventListener;

use JDare\ClankBundle\Event\ClientEvent;
use JDare\ClankBundle\Event\ClientErrorEvent;

class ClientEventListener
{
    protected $redis;

    public function __construct($redis){
        $this->redis = $redis;
    }

    /**
     * Called whenever a client disconnects
     *
     * @param ClientEvent $event
     */
    public function onClientDisconnect(ClientEvent $event)
    {
        $conn = $event->getConnection();
        $this->redis->srem('chat:connected:user', $conn->ChatNickname);

        echo $conn->ChatNickname . " disconnected" . PHP_EOL;
    }

}