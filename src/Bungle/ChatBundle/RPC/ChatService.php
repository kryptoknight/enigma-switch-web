<?php

namespace Bungle\ChatBundle\RPC;
use Ratchet\ConnectionInterface as Conn;

class ChatService{
    private $container;
    private $redis;

    public function __construct($redis, $container){
        $this->redis = $redis;
        $this->container = $container;
    }

    public function userRegister(Conn $conn, $arg){
        if(array_key_exists(0, $arg)){
            $this->redis->sadd('chat:connected:user', $arg[0]);
            $conn->ChatNickname = htmlentities($arg[0]);
            return array('Message' => $arg[0].' user added to the chat.'
                        , 'currUser' => $this->redis->smembers('chat:connected:user'));
        }
    }

    public function userRemove(Conn $conn, $arg){
        if(array_key_exists(0, $arg)){
            $this->redis->srem('chat:connected:user', $arg[0]);
            return array('Message' => $arg[0].' user removed from the chat.'
                        ,'currUser' => $this->redis->smembers('chat:connected:user'));
        }
    }

    public function userUpdate(Conn $conn, $arg){
        return array('users' => $this->redis->smembers('chat:connected:user'));
    }

    public function sendMessage(Conn $conn, $payload){
        $message = json_decode($payload[0], true);
        return array("result" => "SENT: '{$message['message']}' to USER: {$message['recipient']}]}");
    }

    /**
     *
     * @param \Ratchet\ConnectionInterface $conn
     * @return type
     */
    public function getOnlineUsers(Conn $conn){
        return array("users" => '{"MARK","ANDRAS","DAVE","MIRKO","TASSILO",}');
    }
    /**
     *
     */
    public function imOnline(){

    }
}
?>
