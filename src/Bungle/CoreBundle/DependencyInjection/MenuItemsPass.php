<?php

namespace Bungle\CoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class MenuItemsPass implements CompilerPassInterface
{
    
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('bungle.menu');
        
        foreach ($container->findTaggedServiceIds('menu_item') as $id => $attributes) {
            $definition->addMethodCall('addEntry', array(new Reference($id)));
        }
    }

}