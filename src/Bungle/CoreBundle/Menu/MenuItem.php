<?php

namespace Bungle\CoreBundle\Menu;

class MenuItem
{

	protected $url;
	protected $name;
	protected $parent;
	protected $block;
        protected $role;

	protected $subMenu = array();

	public function __construct($name, $url, $block = null, $role = 'ROLE_USER'){
		$this->name = $name;
		$this->url = $url;
        $this->block = $block;
        $this->role = $role;
	}

	public function getName(){
		return $this->name;
	}

	public function getUrl(){
		return $this->url;
	}

	public function getSubMenu()
    {
        return $this->subMenu;
    }

    public function getBlock(){
    	return $this->block;
    }

	public function addSubmenuEntry(MenuItem $item)
    {
        if (!isset($this->subMenu[$item->getName()]))
        {
            $this->subMenu[$item->getName()] = array();
        }        
        
        $this->subMenu[$item->getName()] = $item;
        
        $item->setParent($this);
        
        //$this->sortItems();
    }

	public function createSubmenuEntry($name, $url, $role = null)
    {
        $tmp = new MenuItem($name, $url, null, $role?:$this->role);
        
        $this->addSubmenuEntry($tmp);
        
        return $tmp;
    }

    public function setParent($v)
    {
        $this->parent = $v;
    }

    public function getSubmenuEntry($name)
    {        
        return @$this->subMenu[$name];
    }

    public function isZero()
    {
        return count($this->subMenu) == 0;
    }

    public function getRole()
    {
        return $this->role;
    }

}