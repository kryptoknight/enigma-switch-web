<?php

namespace Bungle\CoreBundle\Menu;

use Symfony\Component\DependencyInjection\ContainerAware;


class Menu extends ContainerAware {
    
    
    protected $root;
    
    public function __construct()
    {
        $this->root = new MenuItem('root', '');
    }
    
    public function addEntry(MenuItem $item)     {

        $block = $item->getBlock();
        $role = $item->getRole();
        $notExists = !$this->root->getSubmenuEntry($block);


        if ( $notExists ) {
            $this->root->createSubmenuEntry($block, '', $role);
        }
        
        $this->root->getSubmenuEntry($block)->addSubmenuEntry($item);
    }
    
    public function getMenu()
    {        
        return $this->root;
    }



}