<?php

namespace Bungle\CoreBundle\Redis;

use Bungle\CoreBundle\Redis\RedisServer;

class RedisConnectionFactory {

    private $socketFile;
    private $dbAliases;
    private $timeout;

    private $connected;

    public function __construct() {
        /*
        later can be from config
        $this->socketFile = $config['socket'];
        $this->dbAliases = $config['dbalias'];
        $this->timeout = $config['timeout'];
        $this->connected = array();
         */
        $this->socketFile = '/var/run/redis/redis.sock';
        $this->dbAliases = array('chat' => array('db' => 1)
                                ,'cache' => array('db' => 2)
                                );
        $this->timeout = 3600;
        $this->connected = array();
    }

    public function connect($dbAlias) {

        if ( !isset($this->connected[$dbAlias]) ) {
            $this->connected[$dbAlias] = new RedisServer();
        }
        return $this->connected[$dbAlias];
    }


}
