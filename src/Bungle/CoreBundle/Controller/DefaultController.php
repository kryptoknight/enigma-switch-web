<?php

namespace Bungle\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bungle\CompanyBundle\Document\Company;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="bungle_core")
     * @Route("/index.html", name="index")
     * @Template()
     */
    public function indexAction(){

        $response = $this->render('BungleCoreBundle:Default:index.html.twig', array('success'=>"true"));
        $response->setPublic();
        //$response->setSharedMaxAge(86400);

        return $response;
    }

    /**
     * @Route("/setup/{password}")
     * @Template()
     */
    public function setupAction($password){
        if ($password == "jtc03291"){
            $dm = $this->container->get('doctrine.odm.mongodb.document_manager');

            // Create a Default Company.
            $defaultCompany = $this->container->getParameter('bungle_company.default_company');
            $Company = new \Bungle\CompanyBundle\Document\Company();
            $CompanyRepo = $dm->getRepository('Bungle\CompanyBundle\Document\Company');
            $Company->setName($defaultCompany);
            $dm->persist($Company);
            $dm->flush();

            // Create an admin user
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->createUser();
            $user->setUsername('mdalby');
            $user->setEnabled(true);
            $user->setPlainPassword("Jtc03291!");
            $userManager->updateUser($user);
            $user = new \Bungle\UserBundle\Document\User();
            $userRepo = $dm->getRepository('Bungle\UserBundle\Document\User');
            $user = $userRepo->FindOneBy(array('username'=>'mdalby'));
            $user->setEmail("mark@dalbymail.com");
            $user->setName('Administrator');
            $user->setCompany($CompanyRepo->FindOneBy(array('name'=>$defaultCompany)));
            $user->addRole("GOD");
            $user->setSuperAdmin(true);
            $dm->persist($user);
            $dm->flush();

            return array('success'=>"true");
        }else{
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }
    }
}
