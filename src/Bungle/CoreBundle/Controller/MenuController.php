<?php

namespace Bungle\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends Controller {
    
    public function menuAction() {
        $menu = $this->container->get('bungle.menu')->getMenu();

        return $this->render('BungleCoreBundle:Menu:menu.html.twig', array('menu' => $menu));
    }
}