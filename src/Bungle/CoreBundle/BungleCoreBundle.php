<?php

namespace Bungle\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Bungle\CoreBundle\DependencyInjection\MenuItemsPass;


class BungleCoreBundle extends Bundle
{

	public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new MenuItemsPass());
    }    
}
