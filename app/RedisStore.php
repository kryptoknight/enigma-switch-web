<?php

/**
 * Store implements all the logic for storing cache metadata (Request and Response headers).
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class RedisStore implements \Symfony\Component\HttpKernel\HttpCache\StoreInterface
{
    private $root;
    private $keyCache;
    private $locks;

    private $Redis;
    private $rStart;
    /**
     * Constructor.
     *
     * @param string $root The path to the cache directory
     */
    public function __construct($root) {

        $this->Redis = new \Bungle\CoreBundle\Redis\RedisServer();
        $this->rStart = microtime(true);
        $this->keyCache = new \SplObjectStorage();
        $this->locks = array();
    }
    public function getRedis() {
        return $this->Redis;
    }
    /**
     * Cleanups storage.
     */
    public function cleanup() {
        foreach ($this->locks as $lock) {
            $this->Redis->del($lock);
        }

        $error = error_get_last();
        if (1 === $error['type'] && false === headers_sent()) {
            // send a 503
            header('HTTP/1.0 503 Service Unavailable');
            header('Retry-After: 10');
            echo '503 Service Unavailable';
        }
    }

    /**
     * Locks the cache for a given Request.
     *
     * @param Request $request A Request instance
     *
     * @return Boolean|string true if the lock is acquired, the path to the current lock otherwise
     */
    public function lock(\Symfony\Component\HttpFoundation\Request $request)  {

        $cacheKey = $this->getCacheKey($request);
        $cacheKeyLock = $cacheKey.'lck';

        if ( $this->Redis->setnx($cacheKeyLock,$this->rStart) ) {
            $this->locks[] = $cacheKeyLock;
            return true;
        }
        return $cacheKeyLock;
    }

    public function isLocked($lock) {
        return $this->Redis->exists($lock);
    }

    /**
     * Releases the lock for the given Request.
     *
     * @param Request $request A Request instance
     */
    public function unlock(\Symfony\Component\HttpFoundation\Request $request)
    {
        $cacheKey = $this->getCacheKey($request);
        $cacheKeyLock = $cacheKey.'lck';
        $this->Redis->del($cacheKeyLock);
    }

    /**
     * Locates a cached Response for the Request provided.
     *
     * @param Request $request A Request instance
     *
     * @return Response|null A Response instance, or null if no cache entry was found
     */
    public function lookup(\Symfony\Component\HttpFoundation\Request $request)
    {
        $key = $this->getCacheKey($request);

        if (!$entries = $this->getMetadata($key)) {
            return null;
        }

        // find a cached entry that matches the request.
        $match = null;
        foreach ($entries as $entry) {
            if ($this->requestsMatch(isset($entry[1]['vary']) ? $entry[1]['vary'][0] : '', $request->headers->all(), $entry[0])) {
                $match = $entry;

                break;
            }
        }

        if (null === $match) {
            return null;
        }

        list($req, $headers) = $match;

        $cacheKey = ($headers['x-content-digest'][0]);

        if ( $this->Redis->exists($cacheKey) ) {
            return $this->restoreResponse($headers, $cacheKey);
        }
        // TODO the metaStore referenced an entity that doesn't exist in
        // the entityStore. We definitely want to return nil but we should
        // also purge the entry from the meta-store when this is detected.
        return null;
    }

    protected function getStoredDigest($key) {
        if (!$entries = $this->getMetadata($key)) {
            return null;
        }
        list($req, $headers) = $entries[0];
        return  $headers['x-content-digest'][0];
    }

    /**
     * Writes a cache entry to the store for the given Request and Response.
     *
     * Existing entries are read and any that match the response are removed. This
     * method calls write with the new list of cache entries.
     *
     * @param Request  $request  A Request instance
     * @param Response $response A Response instance
     *
     * @return string The key under which the response is stored
     */
    public function write(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
        $key = $this->getCacheKey($request);
        $storedEnv = $this->persistRequest($request);
        $digest ='?';
        // write the response body to the entity store if this is the original response
        if (!$response->headers->has('X-Content-Digest')) {
            //$digest = 'en'.sha1($response->getContent());
            $digest = 'content:'.$key.':'.sha1($response->getContent());

            if (false === $this->save($digest, $response->getContent())) {
                throw new \RuntimeException('Unable to store the entity.');
            }

            $response->headers->set('X-Content-Digest', $digest);

            if (!$response->headers->has('Transfer-Encoding')) {
                $response->headers->set('Content-Length', strlen($response->getContent()));
            }
        }

        // read existing cache entries, remove non-varying, and add this one to the list
        $entries = array();
        $vary = $response->headers->get('vary');
        foreach ($this->getMetadata($key) as $entry) {
            if (!isset($entry[1]['vary'])) {
                $entry[1]['vary'] = array('');
            }

            if ($vary != $entry[1]['vary'][0] || !$this->requestsMatch($vary, $entry[0], $storedEnv)) {
                $entries[] = $entry;
            }
        }

        $headers = $this->persistResponse($response);
        unset($headers['age']);

        array_unshift($entries, array($storedEnv, $headers));
        if ('?' !== $digest) {
            $prevDigest = $this->getStoredDigest($key);
            if ( !is_null($prevDigest) ) {
                if ($prevDigest == $digest) {
                   // error_log('K:'.$key.'=>'.$prevDigest."\n",3,'/dev/shm/cache-write.log');
                }
                else {
                    $this->Redis->del($prevDigest); // ha ez nem jó akkor csak ttl-t vegyük le
                   // error_log('W:'.$key.'=>'.$prevDigest.'=>'.$digest."\n",3,'/dev/shm/cache-write.log');
                }
            }
            else {
               // error_log('N:'.$key.'=>NULL=>'.$digest."\n",3,'/dev/shm/cache-write.log');
            }
        }

        if (false === $this->save($key, serialize($entries))) {
            throw new \RuntimeException('Unable to store the metadata.');
        }

        return $key;
    }

    /**
     * Invalidates all cache entries that match the request.
     *
     * @param Request $request A Request instance
     */
    public function invalidate(\Symfony\Component\HttpFoundation\Request $request)
    {
        $modified = false;
        $key = $this->getCacheKey($request);

        $entries = array();
        foreach ($this->getMetadata($key) as $entry) {
            $response = $this->restoreResponse($entry[1]);
            if ($response->isFresh()) {
                $response->expire();
                $modified = true;
                $entries[] = array($entry[0], $this->persistResponse($response));
            } else {
                $entries[] = $entry;
            }
        }

        if ($modified) {
            if (false === $this->save($key, serialize($entries))) {
                throw new \RuntimeException('Unable to store the metadata.');
            }
        }

        // As per the RFC, invalidate Location and Content-Location URLs if present
        foreach (array('Location', 'Content-Location') as $header) {
            if ($uri = $request->headers->get($header)) {
                $subRequest = Request::create($uri, 'get', array(), array(), array(), $request->server->all());

                $this->invalidate($subRequest);
            }
        }
    }

    /**
     * Determines whether two Request HTTP header sets are non-varying based on
     * the vary response header value provided.
     *
     * @param string $vary A Response vary header
     * @param array  $env1 A Request HTTP header array
     * @param array  $env2 A Request HTTP header array
     *
     * @return Boolean true if the the two environments match, false otherwise
     */
    private function requestsMatch($vary, $env1, $env2)
    {
        if (empty($vary)) {
            return true;
        }

        foreach (preg_split('/[\s,]+/', $vary) as $header) {
            $key = strtr(strtolower($header), '_', '-');
            $v1 = isset($env1[$key]) ? $env1[$key] : null;
            $v2 = isset($env2[$key]) ? $env2[$key] : null;
            if ($v1 !== $v2) {
                return false;
            }
        }

        return true;
    }

    /**
     * Gets all data associated with the given key.
     *
     * Use this method only if you know what you are doing.
     *
     * @param string $key The store key
     *
     * @return array An array of data associated with the key
     */
    private function getMetadata($key)
    {
        if (false === $entries = $this->load($key)) {
            return array();
        }

        return unserialize($entries);
    }

    /**
     * Purges data for the given URL.
     *
     * @param string $url A URL
     *
     * @return Boolean true if the URL exists and has been purged, false otherwise
     */
    public function purge($url)
    {
        $cacheKey = $this->getCacheKey(\Symfony\Component\HttpFoundation\Request::create($url));
        if ($this->Redis->exists($cacheKey) ) {
            $this->Redis->del($cacheKey);
            return true;
        }
        return false;
    }

    /**
     * Loads data for the given key.
     *
     * @param string $key  The store key
     *
     * @return string The data associated with the key
     */
    public function load($key)
    {
        if ($this->Redis->exists($key) ) {
            return $this->Redis->get($key);
        }
        return false;
    }

    /**
     * Save data for the given key.
     *
     * @param string $key  The store key
     * @param string $data The data to store
     */
    private function save($key, $data)  {
        $this->Redis->setex($key,84600,$data);
    }


    /**
     * Returns a cache key for the given Request.
     *
     * @param Request $request A Request instance
     *
     * @return string A key for the given Request
     */
    private function getCacheKey(\Symfony\Component\HttpFoundation\Request $request)
    {
        if (isset($this->keyCache[$request])) {
            return $this->keyCache[$request];
        }

        //return $this->keyCache[$request] = 'md'.sha1($request->getUri());
        return $this->keyCache[$request] = 'uri:'.($request->getUri());
    }

    /**
     * Persists the Request HTTP headers.
     *
     * @param Request $request A Request instance
     *
     * @return array An array of HTTP headers
     */
    private function persistRequest(\Symfony\Component\HttpFoundation\Request $request)
    {
        return $request->headers->all();
    }

    /**
     * Persists the Response HTTP headers.
     *
     * @param Response $response A Response instance
     *
     * @return array An array of HTTP headers
     */
    private function persistResponse(\Symfony\Component\HttpFoundation\Response $response)
    {
        $headers = $response->headers->all();
        $headers['X-Status'] = array($response->getStatusCode());

        return $headers;
    }

    /**
     * Restores a Response from the HTTP headers and body.
     *
     * @param array  $headers An array of HTTP headers for the Response
     * @param string $body    The Response body
     */
    private function restoreResponse($headers, $body = null)
    {
        $status = $headers['X-Status'][0];
        unset($headers['X-Status']);

        if (null !== $body) {
            $headers['X-Body-File'] = array($body);
        }

        return new \Symfony\Component\HttpFoundation\Response($body, $status, $headers);
    }


    /*-IndexPageExtension-*/
    public function refreshIndexPage() {
        /*
            ha van refreshIndex:'', de nincs refreshIndexDelayed
        */
        return $this->Redis->exists('admin-refreshIndex') && !$this->Redis->exists('refreshIndexDelayed');
    }

    public function indexPageExpired() {
        /*
            ha van refreshIndex:'', de nincs refreshIndexDelayed
        */
        $this->Redis->del('admin-refreshIndex');
    }
}
