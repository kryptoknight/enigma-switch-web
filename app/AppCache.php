<?php

require_once __DIR__.'/AppKernel.php';


require_once __DIR__.'/KernelHttpCache.php';
require_once __DIR__.'/HttpCache.php';
require_once __DIR__.'/RedisStore.php';

use Symfony\Bundle\FrameworkBundle\HttpCache\HttpCache;
use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Client;

use Bungle\CoreBundle\Redis\RedisServer;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AppCache extends HttpCache
{
    public function __construct(\Symfony\Component\HttpKernel\HttpKernelInterface $kernel)
    {
        parent::__construct($kernel);
    }

}
